/**
 * Classe Client
 * @author Chokote Leondaris
 * @version 1.2
 * */

public class Client {
	
	
	/** 
	 * Identifiant du client
	 * 
	 *
	 */
	private  double id;
	

	/** 
	 * nom de l'entreprise
	 */
	
	
	private  String nomEntreprise;
	
	/** 
	 * le numéro Siret de l'entreprise
	 */ 
	
	private long numSiret;
	
	/** 
	 * adresse de l'entreprise
	 */
	
	private String adresseEntre;
	
	/** 
	 * code ape de l'entreprise
	 */
	
	private String codeApe; 
	

	/**
	 * Constructeur
	 * @param identifiant du client
	 * @param nom de l'entreprise
	 * @param numéro Siret de l'entreprise
	 * @param adresse de l'entreprise
	 * @param code ape de l'entreprise
	 */
	
	public Client (double id,String nomEntreprise,long numSiret, String adresseEntre, String codeApe){
		this.id=id;
		this.nomEntreprise= nomEntreprise;
		this.numSiret= numSiret;
		this.adresseEntre=adresseEntre; 
		this.codeApe=codeApe;
		
	}
	
	// les getteurs 
	
	/**
	 * getter pour l'attribut identifiant
	 * @return identifiant
	 * 
	 */
	
	public double  getId (){
		return id;
	}
	
	
	/**
	 * getter pour l'attribut nom de l'entreprise
	 * @return nom de l'entreprise
	 */
	public String getNomEntreprise(){
		return nomEntreprise;
	}
	
	/**
	 * getter pour l'attribut numéro de Siret
	 * @return le numéro de SIret de l'entreprise
	 */
	
	public long getNumSiret(){
		return numSiret;
	}
	
	
	/**
	 * getter pour l'attribut adresseENtre
	 * @return l'adresse de l'entreprise
	 */ 
	
	public String getAdresseEntre(){
		return adresseEntre;
	}
	
	/**
	 * getter pour l'attribut codeApe
	 * @return le code APE de l'entreprise
	 */
	public String getCodeApe(){
		return codeApe;
	}
	
	
	// les setteurs
	
	/**
	 * setter  pour l'attributid
	 * @param   nouveau identifiant du client
	 */
	
	public void setId (double  id){
		this.id=id;
	}
	
	
	/**
	 * setter  pour l'attribut nomEntreprise
	 * @param   nouveau nom de l'entreprise
	 */
	
	public void setNomEntreprise(String nomEntreprise){
		this.nomEntreprise= nomEntreprise;
	}
	
	/**
	 * setter  pour l'attribut numSiret
	 * @param   nouveau numéro SIRET
	 */
	
	
	public void setNumSiret (long numSiret){
		this.numSiret= numSiret;
		
	}
	
	/**
	 * setter  pour l'attribut adresseEntre
	 * @param   nouvelle adresse de l'entreprise
	 */
	
	public void setAdresseEntre(String adresseEntre){
		this.adresseEntre=adresseEntre; 
		
	}
	
	/**
	 * setter  pour l'attribut codeApe
	 * @param   nouveau code APE
	 */
	
	public void setcodeApe(String codeApe){
		this.codeApe=codeApe;
		
	}
	
	/**
	 * Redéfinition de la méthode toString permettant de définir la traduction de l'objet en String
	 * pour l'affichage par exemple
	 */
	
	public String toString() {
		return "Client : " + nomEntreprise + " - " + numSiret
				+ ", " + adresseEntre + "- " + codeApe; 
	}

	

	

	
	
}
