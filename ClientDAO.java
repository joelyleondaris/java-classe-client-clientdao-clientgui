import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'accès aux données contenues dans la table client
 * 
 * @author Chokote Léondaris
 * @version 1.2
 * */
public class ClientDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "BDD3";  //exemple BDD1
	final static String PASS = "BDD3";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public ClientDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un client dans la table client Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param client
	 *            le client à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(Client client) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			ps = con.prepareStatement("INSERT INTO client_clt (id_clt,ent_clt, siret_clt, adr_clt, ape_clt) VALUES (?, ?, ?, ?, ?)");
			ps.setDouble (1, client.getId());
			ps.setString(2, client.getNomEntreprise());
			ps.setLong(3, client.getNumSiret());
			ps.setString(4, client.getAdresseEntre());
			ps.setString(5, client.getCodeApe());

			// Exécution de la requête
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer un client à partir du numero de SIRET
	 * 
	 * @param numSiret
	 *            le numéro de Siret du client à récupérer 
	 * @return 	le client trouvé;
	 * 			null si aucun client ne correspond à ce numéro de SIRET
	 */
	public Client getClient(long numSiret) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Client retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM client_clt WHERE siret_clt = ?");
			ps.setLong(1, numSiret);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new Client(rs.getDouble("id_clt"),
						rs.getString("ent_clt"),
						rs.getLong("siret_clt"),
						rs.getString("adr_clt"), rs.getString("ape_clt"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les clients stockés dans la table client_clt
	 * 
	 * @return une ArrayList de Client
	 */
	public List<Client> getListeClient() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Client> retour = new ArrayList<Client>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM client_clt");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Client(rs.getDouble("id_clt"), rs
						.getString("ent_clt"), rs
						.getLong("siret_clt"), rs
						.getString("adr_clt"), rs.getString("ape_clt")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		ClientDAO clientDAO = new ClientDAO();
		// test de la méthode ajouter
		Client a1 = new Client(1, "Samsung", 12365478523641L, "2 avenue Georges Paris" , "8546H");
		int retour = clientDAO.ajouter(a1);

		System.out.println(retour + " lignes ajoutées");

		// test de la méthode getClient
		Client a2 = clientDAO.getClient(1);
		System.out.println(a2);

		// test de la méthode getListeClient
		List<Client> liste = clientDAO.getListeClient();
		// affichage des articles
		for (Client clt : liste) {
			System.out.println(clt.toString());
		}

	}
}
