

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


/**
 * Classe ClientFenetre
 * Définit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouveau client dans la table client via
 * la saisie des identifiant des clients, le nom de l'entreprise, le numero siret, 
 * l'adresse de l'entreprise, le codr ape du client
 *    - Permet l'affichage de tous les clients dans la console
 *    
 * @author Chokote & Leondaris
 * @version 1.3
 * */


public class ClientGUI extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ClientFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ClientFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ identifiant Client
	 */
	private JTextField textFieldId;

	/**
	 * zone de texte pour le champ nom entreprise
	 */
	private JTextField textFieldNomEntreprise;

	/**
	 * zone de texte pour le numéro de siret
	 * 
	 */
	private JTextField textFieldNumSiret;
	/**
	 * zone de texte pour l'adresse de l'entreprise
	 * 
	 */
	private JTextField textFieldAdresseEntre;
	/**
	 * zone de texte pour le code Ape
	 * 
	 */
	private JTextField textFieldCodeApe;
	
	private JTextField textFieldRecherche;

	
	
	/**
	 * label identifiant du client
	 */
	private JLabel labelId;

	/**
	 * label nom de l'entreprise
	 */
	private JLabel labelNomEntreprise;

	/**
	 * label numéro de siret
	 */
	private JLabel labelNumSiret;

	/**
	 * label adresse de l'entreprise
	 */
	private JLabel labelAdresseEntre;
	
	/**
	 * label code ape
	 */
	private JLabel labelCodeApe;
	
	private JLabel labelRecherche;
	
	
	

	/**
	 * bouton d'ajout du client
	 */
	private JButton boutonAjouter;
	
	private JButton boutonRecherche;

	/**
	 * bouton qui permet d'afficher tous les clients
	 */
	private JButton boutonAffichageTousLesClients;

	/**
	 * Zone de texte pour afficher les clients
	 */
	private JTextArea zoneTextListClient;
	
	private JTextArea zoneTextClient;

	/**
	 * Zone de défilement pour la zone de texte
	 */
	private JScrollPane zoneDefilement;

	
	
	/**
	 * instance de ClientDAO permettant les accès à la base de données
	 */
	private ClientDAO monClientDAO;

	/**
	 * Constructeur Définit la fenêtre et ses composants - affiche la fenêtre
	 */
	public ClientGUI() {
		// on instancie la classe Client DAO
		this.monClientDAO = new ClientDAO ();
		

		// on fixe le titre de la fenêtre
		this.setTitle("Client");
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);

		// création du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de gérer la position des éléments
		// il autorisera un retaillage de la fenêtre en conservant la
		// présentation
		// BoxLayout permet par exemple de positionner les élements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.PINK);

		// instantiation des composants graphiques
		textFieldId = new JTextField();
		textFieldNomEntreprise = new JTextField();
		textFieldNumSiret = new JTextField();
		textFieldAdresseEntre = new JTextField();
		textFieldCodeApe = new JTextField();
		textFieldRecherche = new JTextField();
		
		boutonAjouter = new JButton("Ajouter");
		boutonRecherche = new JButton ("Rechercher");
		boutonAffichageTousLesClients = new JButton(
				"Afficher tous les clients");
		labelId = new JLabel("Identifiant client :");
		labelNomEntreprise = new JLabel("Nom entreprise :");
		labelNumSiret = new JLabel("Numero siret :");
		labelAdresseEntre = new JLabel("Adresse entreprise :");
		labelCodeApe = new JLabel("Code Ape :");
		labelRecherche = new JLabel ("Rechercher :");

		zoneTextClient = new JTextArea(10, 20);
		zoneTextClient.setEditable(false);
		
		zoneTextListClient = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListClient);
		zoneTextListClient.setEditable(false);

		// ajout des composants sur le container
		containerPanel.add(labelId);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldId);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		
		containerPanel.add(labelNomEntreprise);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNomEntreprise);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelNumSiret);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNumSiret);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelAdresseEntre);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldAdresseEntre);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelCodeApe);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldCodeApe);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		
		containerPanel.add(boutonAjouter);
		
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(labelRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(boutonRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(textFieldRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(zoneTextClient);
		

		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));

		containerPanel.add(boutonAffichageTousLesClients);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(zoneDefilement);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des écouteurs sur les boutons pour gérer les évènements
		boutonAjouter.addActionListener(this);
		boutonAffichageTousLesClients.addActionListener(this);

		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fenêtre
		this.setVisible(true);
	}

	/**
	 * Gère les actions réalisées sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ArticleDAO

		try {
			if (ae.getSource() == boutonAjouter) {
				// on crée l'objet message
				Client a = new Client(
						Double.parseDouble(this.textFieldId.getText()),
						this.textFieldNomEntreprise.getText(),
						Long.parseLong(this.textFieldNumSiret.getText()),
						this.textFieldAdresseEntre.getText(),
						this.textFieldCodeApe.getText());
				// on demande à la classe de communication d'envoyer le client
				// dans la table Client
				retour = monClientDAO.ajouter(a);
				// affichage du nombre de lignes ajoutées
				// dans la bdd pour vérification
				System.out.println("" + retour + " ligne ajoutée ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "Client ajouté !");
				else
					JOptionPane.showMessageDialog(this, "Erreur ajout client",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonAffichageTousLesClients) {
				// on demande à la classe ClientDAO d'ajouter le message
				// dans la base de données
				List<Client> liste = monClientDAO.getListeClient();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListClient.setText("");
				// on affiche dans la console du client les client reçus
				for (Client a : liste) {
					zoneTextListClient.append(a.toString());
					zoneTextListClient.append("\n");
					// Pour afficher dans la console :
					// System.out.println(a.toString());
				}
			} else if (ae.getSource() == boutonRecherche){
				
				zoneTextClient.append(monClientDAO.getClient(Long.parseLong(this.textFieldRecherche.getText())).toString());
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez contrôler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contrôler vos saisies");
		}

	}

	public static void main(String[] args) {
		new ClientGUI ();
	}

}